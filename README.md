
Use this project to generate Typescript objects from C# objects.

**NOTE**: Prefered use is to have a .net standard library that contains only the objects you whish to convert.


## To use. 
### In your c# project 

set all wanted types to implement "ITypeScriptEntity"

Example:
#
    public class MyObject : ITypeScriptEntity
    {
		public string Id {get;set;}
    }
	
	
Then add the build scripts to the .csproj file.
Make your .csproj look something like this:
#
**NOTE**: Make sure DLLName,Destination, and GenieVersion are correct
#
    <PropertyGroup>
      <TargetFramework>netstandard2.0</TargetFramework>
      <RunPostBuildEvent>OnOutputUpdated</RunPostBuildEvent>
    </PropertyGroup>
    
    <ItemGroup>
      <PackageReference Include="TypeScriptGenie" Version="1.0.X" />
    </ItemGroup>
    
    
    <Target Name="PostBuild" AfterTargets="PostBuildEvent" Condition="'$(Configuration)' == 'Debug'">
     
       <PropertyGroup>
        <DLLName>MyModels.dll</DLLName>
        <Destination>$(MSBuildProjectDirectory)\..\AngularAPP\TypescriptModels</Destination>
		<UseTitleCase>false</UseTitleCase>
        <GenieVersion>1.0.X</GenieVersion>
        <SourceFile>$(MSBuildProjectDirectory)\bin\$(Configuration)\$(TargetFramework)\$(DLLName)</SourceFile>
        <GenieExe>"$(NuGetPackageRoot)typescriptgenie\$(GenieVersion)\lib\netstandard2.0\TypeScriptGenie.Generator.dll"</GenieExe>
        
      </PropertyGroup>
    
      <Exec Command="dotnet $(GenieExe) source=$(SourceFile)  destination=$(Destination) useTitleCase=$(UseTitleCase)" IgnoreExitCode="false"/>
    
    </Target>

Build your project in debug mode.
You should then see a new folder named as your Destination foler


