﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.IO;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Runtime.Loader;

namespace TypeScriptGenie
{
    class Program
    {

        static void Main(string[] args)
        {


            string sourceFile = null;

            string destinationFolder = null;

            bool useTitleCaseProperties = false;
            foreach (var arg in args)
            {
                var split = arg.Split('=');



                if (split.Length < 2)
                {
                    throw new ArgumentException($"Unknown {arg}");
                }

                if (split[0].ToLower() == "source")
                {
                    sourceFile = split[1];
                }

                else if (split[0].ToLower() == "destination")
                {
                    destinationFolder = split[1];
                }
                else if (split[0].ToLower() == "usetitlecase")
                {
                    Boolean.TryParse(split[1], out useTitleCaseProperties);
                    continue;
                }
                else
                {
                    throw new ArgumentException($"Unknown split[1] \n expected 'source=' or 'destination='");
                }


            }


            if (string.IsNullOrEmpty(sourceFile))
            {
                throw new ArgumentException($"Missing Source Dll File argument 'source='");
            }
            if (string.IsNullOrEmpty(destinationFolder))
            {
                throw new ArgumentException($"Missing Destination Folder argument 'destination='");
            }




            var loader = new AssemblyLoader(sourceFile);

            var types = loader.GetEntityTypes();


            if (types == null || types.Count == 0)
            {
                throw new Exception("No Entity Types found");
            }

            Console.WriteLine($"Found {types.Count} entities to convert");



            var assemblyName = loader.GetAssemblyname();
            var loadedAssembly = loader.GetLoadedAssembly();

            TypeScriptFileMaker.Generate(types, destinationFolder, assemblyName, loadedAssembly, useTitleCaseProperties);

        }






    }
}
