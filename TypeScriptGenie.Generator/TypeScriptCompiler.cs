﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace TypeScriptGenie
{
    public static class TypeScriptCompiler
    {

        public static void Compile(string workingDirectory)
        {
            //Install NPM
            Console.WriteLine("Installing npm");

            Process cmd = new Process();
            cmd.StartInfo.FileName = Platform.GetCurrentExe();

            cmd.StartInfo.WorkingDirectory = workingDirectory;
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();

            cmd.StandardInput.WriteLine("npm i");
            cmd.StandardInput.Flush();

            cmd.StandardInput.Close();
            cmd.WaitForExit();

            Console.WriteLine(cmd.StandardOutput.ReadToEnd());


            //Compile typescript
            Console.WriteLine("Compiling TSC");
            cmd = new Process();
            cmd.StartInfo.FileName = Platform.GetCurrentExe();
            cmd.StartInfo.WorkingDirectory = workingDirectory;
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.StandardInput.WriteLine("npm run build");
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            Console.WriteLine(cmd.StandardOutput.ReadToEnd());
        }

    }
}
