﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using Microsoft.Extensions.DependencyModel.Resolution;
using Microsoft.Extensions.DependencyModel;


namespace TypeScriptGenie
{
    internal  class AssemblyLoader
    {
        private FileInfo _dllFile;
        private DirectoryInfo _binDirectory;

        private readonly ICompilationAssemblyResolver _assemblyResolver;
        private readonly DependencyContext _dependencyContext;
        private Assembly _loadedAssembly;

        private readonly AssemblyLoadContext _loadContext;

        internal AssemblyLoader(string dllFilePath)
        {
            //first make sure we have a good path

            if (string.IsNullOrEmpty(dllFilePath))
                throw new ArgumentNullException("No dll file path specified");


            _dllFile = new FileInfo(dllFilePath);

            if (!_dllFile.Exists)
                throw new ArgumentNullException("No assembly found at path " + dllFilePath);

            _binDirectory = _dllFile.Directory;


            //add the assembly resolver
            //this handles grabbing any referenced files


            _loadedAssembly = AssemblyLoadContext.Default.LoadFromAssemblyPath(_dllFile.FullName);


            var libraryPaths = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            _dependencyContext = DependencyContext.Load(_loadedAssembly);



            var assemblydirectory = System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase;

            _assemblyResolver = new CompositeCompilationAssemblyResolver
            (new ICompilationAssemblyResolver[]
            {
                new AppBaseCompilationAssemblyResolver(_binDirectory.FullName),
                new AppBaseCompilationAssemblyResolver(Path.GetDirectoryName(assemblydirectory)),
                new ReferenceAssemblyPathResolver(),
                new PackageCompilationAssemblyResolver()
            });
            _loadContext = AssemblyLoadContext.GetLoadContext(_loadedAssembly);

            this._loadContext.Resolving += OnResolving;


        }


        internal string GetAssemblyname()
        {

            var assemblyInfo = _loadedAssembly.GetName();
            return assemblyInfo.Name;
        }


        internal Assembly GetLoadedAssembly()
        {
            return _loadedAssembly;
        }

        internal  List<Type> GetEntityTypes()
        {
          
            return _loadedAssembly.GetTypes().Where(t => typeof(ITypeScriptEntity).IsAssignableFrom(t)).ToList();

        }

        private Assembly OnResolving(AssemblyLoadContext context, AssemblyName name)
        {
            Assembly assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.FullName == name.FullName);

            if (assembly != null)
                return assembly;

            bool NamesMatch(RuntimeLibrary runtime)
            {
                return string.Equals(runtime.Name, name.Name, StringComparison.OrdinalIgnoreCase);
            }

            RuntimeLibrary runtimeLibrary =
                this._dependencyContext.RuntimeLibraries.FirstOrDefault(NamesMatch);
            var compilationLibrary = this._dependencyContext.CompileLibraries.FirstOrDefault(l =>
                string.Equals(l.Name, name.Name, StringComparison.OrdinalIgnoreCase));

            if (runtimeLibrary != null)
            {
                var wrapper = new CompilationLibrary(
                    runtimeLibrary.Type,
                    runtimeLibrary.Name,
                    runtimeLibrary.Version,
                    runtimeLibrary.Hash,
                    runtimeLibrary.RuntimeAssemblyGroups.SelectMany(g => g.AssetPaths),
                    runtimeLibrary.Dependencies,
                    runtimeLibrary.Serviceable);

                var assemblies = new List<string>();
                this._assemblyResolver.TryResolveAssemblyPaths(wrapper, assemblies);
                if (assemblies.Count > 0)
                {
                    return this._loadContext.LoadFromAssemblyPath(assemblies[0]);
                }
                else
                {
                    return this._loadContext.LoadFromAssemblyPath(compilationLibrary.Path);

                }
            }




            return null;
        }

        private Assembly AssemblyResolver(object sender, ResolveEventArgs args)
        {
            // Ignore missing resources
            if (args.Name.Contains(".resources"))
                return null;

            // check for assemblies already loaded
            Assembly assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.FullName == args.Name);
            if (assembly != null)
                return assembly;

            // Try to load by filename - split out the filename of the full assembly name
            // and append the base path of the original assembly (ie. look in the same dir)
            string filename = args.Name.Split(',')[0] + ".dll".ToLower();

            string asmFile = Path.Combine(_binDirectory.FullName, filename);

            try
            {
                return System.Reflection.Assembly.LoadFrom(asmFile);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
